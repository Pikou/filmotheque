import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        api_key: "0308946b18bcad403437e21dab244167",
        language: "fr-FR",
        authentificate: false,
        code: '123'
    },
    mutations: {
        addFilmFilmotheque(state, leFilmId) {
            if (localStorage.getItem("lesFilmsFilmotheque") === null || localStorage.getItem("lesFilmsFilmotheque") === undefined || localStorage.getItem("lesFilmsFilmotheque") === "") {
                let lesFilmsFilmotheque = [];
                lesFilmsFilmotheque.push(leFilmId);
                localStorage.setItem("lesFilmsFilmotheque", JSON.stringify(lesFilmsFilmotheque));

                alert("Filmothèque créée et film ajouté");
            } else {
                let lesFilmsFilmotheque = JSON.parse(localStorage.getItem("lesFilmsFilmotheque"));
                let dejaDansFilmotheque = false;

                // Recherche si le film est déjà dans la filmothèque
                lesFilmsFilmotheque.forEach(function (element) {
                   if (element === leFilmId) {
                       dejaDansFilmotheque = true
                   }
                });

                if (dejaDansFilmotheque) {
                    alert("Film déjà dans votre filmothèque")
                } else {
                    lesFilmsFilmotheque.push(leFilmId);
                    localStorage.setItem("lesFilmsFilmotheque", JSON.stringify(lesFilmsFilmotheque));

                    alert("Film ajouté à votre filmothèque");
                }
            }
        },
        deleteFilmFilmotheque(state, leFilmId) {
            let lesFilmsFilmotheque = JSON.parse(localStorage.getItem("lesFilmsFilmotheque"));

            // Recherche de l'index a supprimer
            let indexToDelete = -1;
            lesFilmsFilmotheque.forEach(function (element, index) {
                if (element === leFilmId) {
                    indexToDelete = index;
                }
            });

            // Suppression de l'id
            lesFilmsFilmotheque.splice(indexToDelete, 1);


            localStorage.setItem("lesFilmsFilmotheque", JSON.stringify(lesFilmsFilmotheque));
            alert("Film supprimé de votre filmothèque");
        },
        createCode(state) {
            localStorage.setItem("code", JSON.stringify('123'))
        },
        editCode(state, newCode) {
            localStorage.setItem("code", JSON.stringify(newCode))
        },
        resetCode(state) {
            localStorage.setItem("code", JSON.stringify('123'))
        },
        getCode() {
            return JSON.parse(localStorage.getItem("code"))
        }
    },
    actions: {}
})
