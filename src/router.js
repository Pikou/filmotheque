import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Film from "./components/Film";
import HelloWorld from "./components/HelloWorld";
import MaFilmotheque from "./views/MaFilmotheque";
import Login from "./views/Login";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/film/:id',
      component: Film
    },
    {
      path: '/template',
      component: HelloWorld
    },
    {
      path: '/ma-filmotheque',
      name: 'ma-filmotheque',
      component: MaFilmotheque
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
  ]
})
