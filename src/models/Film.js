export default class Film {
    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }
    get voteNb() {
        return this._voteNb;
    }

    set voteNb(value) {
        this._voteNb = value;
    }

    get titre() {
        return this._titre;
    }

    set titre(value) {
        this._titre = value;
    }

    get tagline() {
        return this._tagline;
    }

    set tagline(value) {
        this._tagline = value;
    }

    get synopsis() {
        return this._synopsis;
    }

    set synopsis(value) {
        this._synopsis = value;
    }

    get genres() {
        return this._genres;
    }

    set genres(value) {
        this._genres = value;
    }

    get dateDeSortie() {
        return this._dateDeSortie;
    }

    set dateDeSortie(value) {
        this._dateDeSortie = value;
    }

    get affiche() {
        return this._affiche;
    }

    set affiche(value) {
        this._affiche = value;
    }

    get voteMoyenne() {
        return this._voteMoyenne;
    }

    set voteMoyenne(value) {
        this._voteMoyenne = value;
    }

    get error() {
        return this._error;
    }

    set error(value) {
        this._error = value;
    }

    constructor(data) {
        if (typeof data.title === "undefined" || data.title === null || data.title === "")
            this._titre = null;
        else
            this._titre = data.title;

        if (typeof data.tagline === "undefined" || data.tagline === null || data.tagline === "")
            this._tagline = null;
        else
            this._tagline = data.tagline;

        if (typeof data.overview === undefined || data.overview === null || data.overview === "")
            this._synopsis = null;
        else
            this._synopsis = data.overview;

        if (typeof data.genres === undefined || data.genres === null || data.genres === "")
            this._genres = [{"name": "Genre inconnu"}];
        else
            this._genres = data.genres;

        if (typeof data.release_date === undefined || data.release_date == null || data.release_date === "")
            this._dateDeSortie = null;
        else
            this._dateDeSortie = data.release_date;

        if (typeof data.poster_path === undefined || data.poster_path === null || data.poster_path === "")
            this._affiche = null;
        else
            this._affiche = "https://image.tmdb.org/t/p/w500" + data.poster_path;

        if (typeof data.vote_average === undefined || data.vote_average === null || data.vote_average === "")
            this._voteMoyenne = 0;
        else
            this._voteMoyenne = data.vote_average;

        if (data.vote_count === undefined || data.vote_count === null || data.vote_count === "")
            this._voteNb = 0;
        else
            this._voteNb = data.vote_count;

        this._id = data.id;
        this._data = data;
    }

    get dateFR() {
        let mois = this.dateDeSortie.substr(5, 2);
        switch (mois) {
            case "01" :
                mois = "Janvier";
                break;
            case "02" :
                mois = "Février";
                break;
            case "03" :
                mois = "Mars";
                break;
            case "04" :
                mois = "Avril";
                break;
            case "05" :
                mois = "Mai";
                break;
            case "06" :
                mois = "Juin";
                break;
            case "07" :
                mois = "Juillet";
                break;
            case "08" :
                mois = "Aout";
                break;
            case "09" :
                mois = "Septembre";
                break;
            case "10" :
                mois = "Octobre";
                break;
            case "11" :
                mois = "Novembre";
                break;
            case "12" :
                mois = "Décembre";
                break;
        }

        let jour = this.dateDeSortie.substr(8, 2);
        switch (jour) {
            case "01" :
                jour = "1";
                break;
            case "02" :
                jour = "2";
                break;
            case "03" :
                jour = "3";
                break;
            case "04" :
                jour = "4";
                break;
            case "05" :
                jour = "5";
                break;
            case "06" :
                jour = "6";
                break;
            case "07" :
                jour = "7";
                break;
            case "08" :
                jour = "8";
                break;
            case "09" :
                jour = "9";
                break;
        }

        return "" + jour + " " + mois + " " + this.dateDeSortie.substr(0, 4);
    }
}