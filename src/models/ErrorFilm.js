export default class ErrorFilm {
    get status_code() {
        return this._status_code;
    }

    set status_code(value) {
        this._status_code = value;
    }

    get status_message() {
        return this._status_message;
    }

    set status_message(value) {
        this._status_message = value;
    }
    constructor(data) {
        this._status_code = data.status_code;
        this._status_message = data.status_message;
    }
}