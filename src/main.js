import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import bd from 'themoviedb-javascript-library'


new Vue({
  router,
  store,
  bd,
  render: h => h(App)
}).$mount('#app');

window.$ = require('jquery');
window.JQuery = require('jquery');
