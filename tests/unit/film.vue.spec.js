import { createLocalVue, mount } from "@vue/test-utils";
import FilmVue from './../../src/components/Film.vue'
import bd from 'themoviedb-javascript-library'
import Film from "../../src/models/Film";
import ErrorFilm from "../../src/models/ErrorFilm";
import Vuex from "vuex";

const localVue = createLocalVue();

localVue.use(Vuex);

describe("Film.vue", function () {
    let state;
    let store;

    beforeEach(() => {
        state = {
            language: "fr-FR",
            api_key: "0308946b18bcad403437e21dab244167"
        };
        store = new Vuex.Store({
            state
        })
    });

    it("Vérification de l'affectation de la variable id qui est la base pour le rendu d'un film", function () {
        const wrapper = mount(FilmVue, {
            propsData: { leFilmId: "550", add: true },
            store,
            localVue
        });

        expect(wrapper.props('leFilmId')).toBe("550");
        expect(wrapper.html()).toContain("Fight Club")
    });
});

describe("Vérification des requetes et instanciation d'un film", function () {
    it("Vérification de l'objet film après une requete", function () {
        let leTitreExpected = "Fight Club";
        let idExpected = 550;

        let leFilm = null;
        bd.common.language = this.$store.state.language;
        bd.common.api_key = this.$store.state.api_key;
        bd.movies.getById({"id": "550"}, function (data) {
                data = JSON.parse(data);
                leFilm = new Film(data);
            }, function (data) {
                data = JSON.parse(data);
                leFilm = new ErrorFilm(data);
            }
        );

        expect(leFilm.titre).toMatch(leTitreExpected);
        expect(leFilm.id).toMatch(idExpected)
    });
});

describe("Vérification de l'écriture et suppression des données dans le localstorage", function () {
    it('Add & Delete', function () {
        let leFilm = null;
        bd.common.language = this.$store.state.language;
        bd.common.api_key = this.$store.state.api_key;
        bd.movies.getById({"id": "550"}, function (data) {
                data = JSON.parse(data);
                leFilm = new Film(data);
            }, function (data) {
                data = JSON.parse(data);
                leFilm = new ErrorFilm(data);
            }
        );
        this.$store.commit('addFilmFilmotheque', leFilm.id);
        let lesFilms = JSON.parse(localStorage.getItem("lesFilmsFilmotheque"));
        expect(lesFilms.length).toEqual(1);
        expect(lesFilms[0].titre).toMatch("Fight Club");

        this.$store.commit("deleteFilmFilmotheque", leFilm.id);
        expect(lesFilms.length).toEqual(0);
    });
});